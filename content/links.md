---
title: links
description: places where you can find taco!
type: home
---

here's where you can find taco on the internet!

## socials

[cohost](https://chirpbirb.cohost.org/) - [mastodon](https://meow.social/@chirpbirb) - [twitter](https://twitter.com/chirpbirb) - [tumblr](https://chirpbirb.tumblr.com/)

## galleries

[furaffinity](https://www.furaffinity.net/user/chirpbirb)