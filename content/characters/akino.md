---
title: Akino, Tiny Tinkerer
name: Akino Wikifoo
age: mid 20s
race: maned wolf + husky
description: An orange wolf-husky hybrid with a white belly and dark brown paws.
height: 12cm (varies)
---

[tba]

<!--more-->

# early life and education
[to be added]

# career and research
[to be added]

# personal life
[to be added]

# notes and references
[to be added]

# further reading
[to be added]
