---
title: Maricelle, Live-In Librarian
name: Maricelle Kirakai
age: early 20s
race: mouse
description: A brown mouse with glasses who lives in a library.
height: 148cm
---

Maricelle Kirakai is a librarian, scholar, and huge nerd.

<!--more-->

Mari is a brown *Mus musculus*.

Mari normally stands at 12cm (~4") tall, but uses a charmed bracelet to grow to
a larger stature to attend the desk at the library she works and lives in.
(a cloaker, if you prefer)

# early life and education
[to be added]

# career and research
[to be added]

# personal life
[to be added]

# notes and references
[to be added]

# further reading
[to be added]
