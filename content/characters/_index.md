---
title: characters
description: all of the creatures inside of taco's head
---

# characters
{class="text-5xl mb-5"}

|Forename |Surname  |Pronouns|Gender presentation|Genetic ancestry  |Primary feature|Secondary feature                      |Hair style           |Hair color |Age      |Natural Height (cm)|Current Height (cm)|Has A Penis|Has A Vulva|Has Breasts|
|---------|---------|--------|-------------------|------------------|---------------|---------------------------------------|---------------------|-----------|---------|-------------------|-------------------|-----------|-----------|-----------|
|Aeolus   |Viridis  |he/they |bird               |Monk parakeet/lion|Green fur      |Wings, white belly                     |N/A                  |N/A        |🦜       |21                 |21                 |N          |N          |🍗         |
|Akino    |Wikifoo  |he/they |Non-binary         |Wolf/Husky        |Orange fur     |White belly                            |N/A                  |N/A        |20s      |175                |5                  |Y          |N          |N          |
|Blanca   |Grato    |she/her |Feminine           |Snow leopard      |Gray fur       |Black ringed spots                     |Long, flowing hair   |Gray       |40s      |169                |13                 |N          |Y          |Y          |
|Esperanza|Cainama  |she/they|Feminine           |Golden retriever  |Auburn fur     |Floppy ears                            |N/A                  |Brown      |50s      |171                |171                |Y          |N          |Y          |
|Gabriela |Kansai   |she/her |Feminine           |Husky             |Black fur      |White belly                            |Short                |Depends! :3|20s      |165                |12                 |N          |Y          |Y          |
|Jen      |Shanxiu  |he/him  |Masculine          |Lion              |Orange fur     |                                       |Flowing, uncut hair  |Grey black |50s      |180                |180                |Y          |N          |N          |
|Maricelle|Prestaria|she/her |Feminine           |Mouse             |Brown fur      |                                       |Wavy, shoulder length|Brown      |30s      |16                 |168                |N          |Y          |Y          |
|Sestra   |Hitsu    |she/they|Non-binary         |Skaga             |White wool     |Curled ram horns                       |N/A                  |N/A        |30s      |89                 |89                 |N          |Y          |Y          |
|Suneko   |Shinzo   |he/him  |Masculine          |Swan              |White feathers |Orange beak                            |N/A                  |N/A        |30s      |168                |168                |N          |Y          |N          |
|Terra    |Viridis  |he/him  |Masculine          |Pegasus           |Brown coat     |Green extremities (muzzle/hands/hooves)|Feathered short hair |Dark green |40s      |178                |178                |Y          |N          |N          |
{class="max-w-md mb-10"}

# character vignettes
{class="text-5xl mb-10"}
