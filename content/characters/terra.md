---
title: Terra, Equine Veterinarian
name: Terra Viridis
age: early 40s
race: pegasus
description: A brown pegasus with a green muzzle.
height: 178cm
---

Terra Viridis is a veterinarian, researcher, explorer, and father.

<!--more-->

Terra is a pegasus with a coat akin to a brown [Paso](https://en.wikipedia.org/wiki/Peruvian_Paso).
He differs in having dark green coloring on his hands, ankles, and muzzle.
He stands at 1.67m (5'6") tall. His wingspan reaches to 10m (~32') on a good day.

# early life and education
[to be added]

# career and research
[to be added]

# personal life
[to be added]

# notes and references
[to be added]

# further reading
[to be added]
