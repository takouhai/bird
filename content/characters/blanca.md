---
title: Blanca, Academic Mage
name: Blanca Grato
age: late 30s
race: snow leopard
description: A grey snow leopard with dark black spots and grey hair.
height: 162cm
---

Blanca Grato is an experienced mage, literary editor, and spotted kitty.

<!--more-->

Blanca is a gray snow leopard with dark spots. She has long, flowing gray hair.
She stands at 162cm (5'4") tall. Her tail is 30 inches long.

# early life and education
[to be added]

# career and research
[to be added]

# personal life
[to be added]

# notes and references
[to be added]

# further reading
[to be added]
