---
title: Aeolus, Green Catbird
name: Aeolus Viridis
age: young
race: gryphon
description: A green gryphon with a white belly and brown feline paws.
height: 90cm - 30cm
---

Aeolus Viridis is a little green gryphon who loves to snuggle and is happy to
cheer up your day. 💚

<!--more-->

{{< figure src="/img/artwork/aeolus.stickers/11.png" alt="chibi sticker of aeolus - artwork by leeohfox" caption="artwork by [leeohfox](https://www.furaffinity.net/user/leeohfox/)" class="flex justify-center">}}

Aeolus is a hybrid creature with a mixed genetic ancestry between a lion and a quaker parrot. He is 21cm tall, but can also be seen to be at heights of up to 1 meter or as small as a few centimeters. 
He has soft, green fur and plumage with a fuzzy white underbelly.
He has a pretty little tufted brown tail.
He is normally in his feral form, but he can shift into a bipedal form at will.

{{< figure src="/img/artwork/aeolus.yoshikigu.chubchow.greenheart.transparent.png" alt="aeolus in a green yoshi kigu hugging an egg - artwork by chubchow" caption="artwork by [chubchow](https://twitter.com/chubchow)" class="flex justify-center">}}


## early life

Aeolus was born like most gryphons in his world -- out of an egg. However, the odd quirk with his gestation was the fact that his mother was a cow, and his father was a pegasus. It turns out that "experimenting" with potions and forms can lead one to lay fertilized eggs! Who knew? Not Terra or Maggie, it seemed. After being inside of an egg for a few months, Aeolus finally popped out to greet his equine and bovine parents with a happy peep and a satisfied purr.

## personal life
Aeolus spends most of his days lounging in conveniently Aeolus-sized locations (see: fits where he sits) around his mother, Maggie. She is usually working to tend to the farm and sell groceries at the shop they sell their goods at, so Aeolus is usually posted on her shoulder as a dedicated farm supervisor. He does a _lot_ of important work.

## notes and references
[to be added]

## further reading
[to be added]


