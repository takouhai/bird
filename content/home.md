---
title: home
description: taco's furry trash receptable [18+]
type: home
---
i'm busy setting everything up still. sorry for the mess.
trust me, it'll look better soon.

|![](/img/buttons/under_construction.gif)|![](/img/buttons/under_construction.gif)|![](/img/buttons/under_construction.gif)|
|---|---|---|

## things you can look at for now

* [characters](../characters)
* [links](../links)

## things that are coming soon

* [writing](../writing)
* [about](../about)
* [artwork](../artwork)
* [music](../music)

{{< figure src="/img/artwork/aeolus.pixelbird.stardew.png" alt="artwork by concernedape in stardew valley, edit by taco" class="flex justify-center">}}

|![](/img/buttons/lovenow.gif)|![](/img/buttons/powered-by-debian.gif)|![](/img/buttons/srgb-now.png)|![](/img/buttons/budgie.gif)|![](/img/buttons/cool-shades.gif)|![](/img/buttons/cooltxt.gif)|
|---|---|---|---|---|---|---|---|
|![](/img/buttons/emulate.gif)|![](/img/buttons/getmozilla2.gif)|![](/img/buttons/lol.gif)|![](/img/buttons/bestview.gif)|![](/img/buttons/hash_now.gif)|![](/img/buttons/fws.gif)|
|[![](/img/buttons/roly-saynotoweb3.gif)](https://yesterweb.org/no-to-web3/)|