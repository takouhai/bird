---
title: 🚨 hold up 🚨
description: taco's furry trash receptable [🔞]
---
# there's some weird stuff in here.

{{< figure src="/img/artwork/aeolus.sippy.rumwik.png" alt="artwork by rumwik" class="flex justify-center">}}

have a bird to soothe you in these trying times.
### this website's content is indended for "mature audiences".
#### that basically means you should be over 18. like, an adult.
# are you _sure_ you want to continue?

```
🚨 content warning: furry, nsfw, adult themes, 18+ 🚨
```

## [✅ yes, i am an adult](home) | [🔞 no, i am a child](https://www.bluey.tv/)

don't say i didn't warn you! 