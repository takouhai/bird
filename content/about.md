---
title: about
description: but when is taco? and how much does it weigh?
type: home
---

> hey, tsauce. taco here. but when is here? [and how much does it weigh?](https://www.youtube.com/watch?v=P85Fj8m6v84)

|name|pronouns|sexuality|gender|genetic ancestry|age|
|---|---|---|---|---|---|
|taco|he/they|[demisexual](#sexuality)|[non-binary](#gender)|[latine](#genetic-ancestry)|mid 20s|

[wip. more to be added!]

## who?

### taco

hi. i'm taco. i do a lot of weird internet things for work and pleasure. you've stumbled upon a page where i talk about myself very openly to strangers on the internet. but i've had [social media](../links#socials) for years, so i'm used to that.

### aeolus

i go by a lot of names/faces on the internet, some you'll never see. in the furry world i use [aeolus](../characters/aeolus) as my main fursona. i've been called the duolingo owl's cousin multiple times, so you don't need to. you can see the other [characters](../characters) i use, too, but aeolus gets the most screentime.

## what?

yeah, don't worry, i'm confused, too. let's do a quick rundown of what i am, to get us all on the same page...

### pronouns

i use `he/him` or `they/them` pronouns!

### sexuality

i'm [demisexual](https://en.wikipedia.org/wiki/Demisexuality)!

### gender

i'm [non-binary/genderqueer](https://en.wikipedia.org/wiki/Non-binary_gender)! i'm comfortable with you calling me whatever you want, though. i wish to be percieved as a [genderless fluffy bird](#aeolus), most of the time.

### genetic ancestry

i'm latine! my parents were cool enough to be ecuadorian and colombian/venezuelan, so i'm like a gran colombian of latinidad. i'm a first generation immigrant and i've lived in ecuador and peru before for a few years! being latine is a big part of my identity. it's a connection to a world and place that's in my blood. 

<!-- ### the skin i'm in -->

<!-- ## when? -->
<!-- ## where? -->
<!-- ## why? -->
