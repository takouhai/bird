---
title: a new living arrangement
author: \@chirpbirb
publishDate: 2021-03-16T21:03:00
type: home
---

Akino finds himself in a new living situation with his partner.

<!--more-->

### links

* [download](a_new_living_arrangement.pdf) [pdf]
* [furaffinity](https://www.furaffinity.net/view/41075714/)

### changelog
* 2023/06/22 - posted to bird.tacowolf.net
* 2022/06/14 - fixed some text
* 2021/03/16 - initial post